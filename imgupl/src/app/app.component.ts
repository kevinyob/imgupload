import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'imgupl';
  selectedFile: File =null;

  constructor (private http:HttpClient)
  {}

  onFileSelected(event)
  {
   this.selectedFile=<File>event.target.files[0];
  }

onUpload()
{
  const fd=new FormData();
  fd.append('image',this.selectedFile,this.selectedFile.name);
  this.http.post('http:///',fd);

}

public imagePath;
  imgURL: any;
  public message: string;
 
  preview(files) {
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }
}